#ifndef DUNGEON_H
#define DUNGEON_H

#include <stdint.h>

#include "character.h"
#include "util.h"

#define MAP_WIDTH 80
#define MAP_HEIGHT 21

#define MAX_ROOMS 12
#define MIN_ROOMS 6

typedef struct {
	uint8_t x;
	uint8_t y;
	uint8_t w;
	uint8_t h;
} room_t;

typedef struct {
	uint8_t hardness[MAP_HEIGHT][MAP_WIDTH];
	uint8_t pc_sight[MAP_HEIGHT][MAP_WIDTH];
	
	uint16_t room_count;
	room_t* room_list;
	
	uint16_t upstair_count;
	position_t* upstair_list;
	
	uint16_t downstair_count;
	position_t* downstair_list;
  
  position_t player_pos;
  
  character_t *characters[MAP_HEIGHT][MAP_WIDTH];
  
} dungeon_t;

/**
 * Initialize values in the dungeon
 */
void init_dungeon(dungeon_t *d);

/**
 * Generate a random dungeon
 */
void gen_random_dungeon(dungeon_t* d);

/**
 * Generate random rooms in the dungeon, and place them in the provided array.
 * Return the number of rooms actually generated
 */
void generate_rooms(dungeon_t* d);

/**
 * Generate corridors to connect the rooms
 */
void generate_corridors(dungeon_t* d);

/**
 * Connect the two given rooms by a corridor
 */
void connect_rooms(dungeon_t* d, int room1, int room2);

/**
 * Generate stairs in random positions on rooms or corridors
 */
void generate_stairs(dungeon_t* d);

/**
 * Generates monsters in the dungeon
 */
void gen_monsters(dungeon_t *d, int nummon);

/**
 * Initialized the turn heap for the dungeon
 */
void init_turn_heap(dungeon_t *d, heap_t *h);

/**
 * Frees all of the malloc'd pointers in the dungeon struct
 */
void free_dungeon(dungeon_t* d);

/**
 * Initialized the dungeon hardness array
 */
void init_dungeon_hardness(dungeon_t* d);

#endif
