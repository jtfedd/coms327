#include <ncurses.h>
#include <stdint.h>
#include <limits.h>
#include <stdarg.h>

#include "dungeon.h"
#include "draw_dungeon.h"

char monster_chars[] = "0123456789abcdef";

void init_screen() {
	// Initialize ncurses screen
	initscr();
	set_escdelay(0);
	
	// Unbuffered input
  cbreak();
	
	// Don't echo back characters that are typed
  noecho();
	
	// Hide the cursor
	curs_set(0);
	nonl();
	intrflush(stdscr, FALSE);
	
	// Make it so keypad characters do what we expect
	keypad(stdscr, TRUE);
	
	// Clear the screen
  clear();
}

void destroy_screen() {
	endwin();
}

void display_message(char *fmt, ...) {
	move(0, 0);
	clrtoeol();
	
	move(0, 0);
	
	va_list args;
	va_start(args, fmt);
	vwprintw(stdscr, fmt, args);
	va_end(args);
}

char get_map_char(dungeon_t *d, int y, int x) {
	int i;
	
	// Check each position from top down, i.e. check the layers which would
	// render over other layers first.
	
	if (y == 0 || y == (MAP_HEIGHT-1) || x == 0 || x == (MAP_WIDTH-1)) {
		if (y == 0 || y == (MAP_HEIGHT - 1)) return '-';
		else return '|';
	}
	
	if (d->characters[y][x]) {
		return d->characters[y][x]->symbol;
	}
	
	for (i = 0; i < d->upstair_count; i++) {
		if (x == d->upstair_list[i].x && y == d->upstair_list[i].y) {
			return '<';
		}
	}
	
	for (i = 0; i < d->downstair_count; i++) {
		if (x == d->downstair_list[i].x && y == d->downstair_list[i].y) {
			return '>';
		}
	}
	
	for (i = 0; i < d->room_count; i++) {
		room_t *r = d->room_list + i;
		if (x >= r->x && x < r->x + r->w && y >= r->y && y < r->y + r->h) {
			return '.';
		}
	}
	
	if (d->hardness[y][x] == 0) {
		return '#';
	}
	
	return ' ';
}

void draw_dungeon(dungeon_t *d) {
	int i, j;
	
	move(1, 0);
	
	for (i = 0; i < MAP_HEIGHT; i++) {
		for (j = 0; j < MAP_WIDTH; j++) {
			addch(get_map_char(d, i, j));
		}
	}
}

void refresh_screen() {
	refresh();
}

void draw_position(dungeon_t *d, int x, int y) {
	mvaddch(y + 1, x, get_map_char(d, y, x));
}
