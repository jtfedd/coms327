#ifndef UTIL_H
#define UTIL_H

#include <stdint.h>

#define max(x, y) ({ \
	typeof (x) _x = x; \
	typeof (y) _y = y; \
	_x > _y ? _x : _y; \
})

#define min(x, y) ({ \
	typeof (x) _x = x; \
	typeof (y) _y = y; \
	_x < _y ? _x : _y; \
})

/**
 * Generate a random number between min and max, inclusive
**/
int randrange(int min, int max);

/**
 * Probability generator
 */
int randchance(double prob);

/**
 * Returns the sign of a number
 * Or zero if the input is zero
 */
int sign(int x);

typedef struct args {
	int load;
	int save;
	int seed;
	int nummon;
} args_t;

typedef struct {
	uint8_t x;
	uint8_t y;
} position_t;

/**
 * Parses command line arguments
 * Provides default values for arguments that do not exist
 */
void parse_args(int argc, char *argv[], args_t *args);

void print_args(args_t *args);

#endif
