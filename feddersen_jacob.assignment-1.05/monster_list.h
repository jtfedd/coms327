#ifndef MONSTER_LIST_H
#define MONSTER_LIST_H

#include "dungeon.h"

int display_monster_list(dungeon_t *d);

#endif
