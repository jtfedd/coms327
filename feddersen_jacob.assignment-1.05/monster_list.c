#include <stdlib.h>
#include <string.h>
#include <ncurses.h>

#include "monster_list.h"
#include "draw_dungeon.h"

char *header_top_bottom = "********************";
char *header_middle =     "*   monster list   *";

char *eastwest[] = {
	"east",
	"west"
};

char *northsouth[] = {
	"south",
	"north"
};

int display_monster_list(dungeon_t *d) {
	int i, j, k;
	int count = 0;
	for (i = 0; i < MAP_HEIGHT; i++) {
		for (j = 0; j < MAP_WIDTH; j++) {
			if (d->characters[i][j] && d->characters[i][j]->monster) {
				count++;
			}
		}
	}
	
	k = 0;
	
	char **monster_strings = malloc(count * sizeof(char*));
	
	for (i = 0; i < MAP_HEIGHT; i++) {
		for (j = 0; j < MAP_WIDTH; j++) {
			if (d->characters[i][j] && d->characters[i][j]->monster) {
				*(monster_strings+k) = malloc(40 * sizeof(char*));
				int dx = d->player_pos.x - d->characters[i][j]->pos.x;
				int dy = d->player_pos.y - d->characters[i][j]->pos.y;
				
				int horiz = abs(dx);
				int vert = abs(dy);
				
				char *str1 = eastwest[min(1, sign(dx)+1)];
				char *str2 = northsouth[min(1, sign(dy)+1)];
				snprintf(*(monster_strings+k), 39, "%c, %d %s and %d %s", d->characters[i][j]->symbol, horiz, str1, vert, str2);
				k++;
			}
		}
	}
	
	k = 0;
	int shouldExit = 0;
	while(1) {
		for (i = 2; i < 21; i++) {
			for (j = 1; j < 79; j++) {
				mvaddch(i, j, ' ');
			}
		}
		
		mvaddstr(3, (80 - strlen(header_top_bottom)) / 2, header_top_bottom);
		mvaddstr(4, (80 - strlen(header_middle)) / 2, header_middle);
		mvaddstr(5, (80 - strlen(header_top_bottom)) / 2, header_top_bottom);
		
		if (k > 0) {
			mvprintw(7, 38, "(+%d)", k);
		}
		
		for (i = k; i < min(k+5, count); i++) {
			mvaddstr(((i-k) * 2) + 9, (80 - strlen(monster_strings[i])) / 2, monster_strings[i]);
		}
		
		if (k+5 < count) {
			mvprintw(19, 38, "(+%d)", count-(k+5));
		}
		
		refresh_screen();
		
		int key = getch();
		
		if (key == KEY_DOWN) {
			k = max(0, min(count-5, k+1));
		}
		
		if (key == KEY_UP) {
			k = max(0, k-1);
		}
		
		if (key == 27) {
			// Escape key
			break;
		}
		
		if (key == 'q' || key == 'Q') {
			shouldExit = 1;
			break;
		}
	}
	
	for (i = 0; i < count; i++) {
		free(monster_strings[i]);
	}
	free(monster_strings);
	
	return shouldExit;
}