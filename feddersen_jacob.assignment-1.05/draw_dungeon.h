#ifndef DRAW_DUNGEON_H
#define DRAW_DUNGEON_H

#include <stdint.h>

#include "dungeon.h"

#define SCREEN_WIDTH 80
#define SCREEN_HEIGHT 24

/**
 * Draw the dungeon
 */
void init_screen();
void display_message(char *fmt, ...);
void draw_dungeon(dungeon_t *d);
void draw_position(dungeon_t *d, int x, int y);
void refresh_screen();
void destroy_screen();

#endif
