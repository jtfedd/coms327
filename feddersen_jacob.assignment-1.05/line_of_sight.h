#ifndef LINE_OF_SIGHT_H
#define LINE_OF_SIGHT_H

#include <stdint.h>

#include "dungeon.h"

#define VIEW_DIST 10

void calc_line_of_sight(dungeon_t *d, uint8_t result[MAP_HEIGHT][MAP_WIDTH], int x, int y);

#endif
