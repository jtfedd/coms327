set -e

cp 00.rlg327 ~/.rlg327/dungeon
cp 00.rlg327 ~/.rlg327/dungeon_untouched
../feddersen_jacob.assignment-1.04/rlg327 --load --save
echo $(diff ~/.rlg327/dungeon ~/.rlg327/dungeon_untouched)

cp 01.rlg327 ~/.rlg327/dungeon
cp 01.rlg327 ~/.rlg327/dungeon_untouched
../feddersen_jacob.assignment-1.04/rlg327 --load --save
echo $(diff ~/.rlg327/dungeon ~/.rlg327/dungeon_untouched)

cp 02.rlg327 ~/.rlg327/dungeon
cp 02.rlg327 ~/.rlg327/dungeon_untouched
../feddersen_jacob.assignment-1.04/rlg327 --load --save
echo $(diff ~/.rlg327/dungeon ~/.rlg327/dungeon_untouched)

cp 03.rlg327 ~/.rlg327/dungeon
cp 03.rlg327 ~/.rlg327/dungeon_untouched
../feddersen_jacob.assignment-1.04/rlg327 --load --save
echo $(diff ~/.rlg327/dungeon ~/.rlg327/dungeon_untouched)

cp 04.rlg327 ~/.rlg327/dungeon
cp 04.rlg327 ~/.rlg327/dungeon_untouched
../feddersen_jacob.assignment-1.04/rlg327 --load --save
echo $(diff ~/.rlg327/dungeon ~/.rlg327/dungeon_untouched)

cp 05.rlg327 ~/.rlg327/dungeon
cp 05.rlg327 ~/.rlg327/dungeon_untouched
../feddersen_jacob.assignment-1.04/rlg327 --load --save
echo $(diff ~/.rlg327/dungeon ~/.rlg327/dungeon_untouched)

cp 06.rlg327 ~/.rlg327/dungeon
cp 06.rlg327 ~/.rlg327/dungeon_untouched
../feddersen_jacob.assignment-1.04/rlg327 --load --save
echo $(diff ~/.rlg327/dungeon ~/.rlg327/dungeon_untouched)

cp 07.rlg327 ~/.rlg327/dungeon
cp 07.rlg327 ~/.rlg327/dungeon_untouched
../feddersen_jacob.assignment-1.04/rlg327 --load --save
echo $(diff ~/.rlg327/dungeon ~/.rlg327/dungeon_untouched)

cp 08.rlg327 ~/.rlg327/dungeon
cp 08.rlg327 ~/.rlg327/dungeon_untouched
../feddersen_jacob.assignment-1.04/rlg327 --load --save
echo $(diff ~/.rlg327/dungeon ~/.rlg327/dungeon_untouched)

cp 09.rlg327 ~/.rlg327/dungeon
cp 09.rlg327 ~/.rlg327/dungeon_untouched
../feddersen_jacob.assignment-1.04/rlg327 --load --save
echo $(diff ~/.rlg327/dungeon ~/.rlg327/dungeon_untouched)

cp adventure.rlg327 ~/.rlg327/dungeon
cp adventure.rlg327 ~/.rlg327/dungeon_untouched
../feddersen_jacob.assignment-1.04/rlg327 --load --save
echo $(diff ~/.rlg327/dungeon ~/.rlg327/dungeon_untouched)

cp welldone.rlg327 ~/.rlg327/dungeon
cp welldone.rlg327 ~/.rlg327/dungeon_untouched
../feddersen_jacob.assignment-1.04/rlg327 --load --save
echo $(diff ~/.rlg327/dungeon ~/.rlg327/dungeon_untouched)

cp hello.rlg327 ~/.rlg327/dungeon
cp hello.rlg327 ~/.rlg327/dungeon_untouched
../feddersen_jacob.assignment-1.04/rlg327 --load --save
echo $(diff ~/.rlg327/dungeon ~/.rlg327/dungeon_untouched)
