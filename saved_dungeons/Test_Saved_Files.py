import glob, subprocess, os

def run_sync(command):
	return subprocess.run(command.split(' '), stdout=subprocess.PIPE).stdout.decode('utf-8')

for file in glob.glob("*.rlg327"):
    print(file)
    
    run_sync('cp ' + file + ' ' + os.environ['HOME'] + '/.rlg327/dungeon')
    run_sync('cp ' + file + ' ' + os.environ['HOME'] + '/.rlg327/dungeon_untouched')
    
    result = run_sync('../feddersen_jacob.assignment-1.06/rlg327 --load --save')
    diff = run_sync('diff ' + os.environ['HOME'] + '/.rlg327/dungeon ' + os.environ['HOME'] + '/.rlg327/dungeon_untouched');
    
    if (diff != ""):
      print("ERROR!");
    else:
      print("File checks out!");
