import subprocess, os

def run_sync(command):
	return subprocess.run(command.split(' '), stdout=subprocess.PIPE).stdout.decode('utf-8')

content = []

with open('distance_maps.txt') as f:
	content = f.read()

data = content.split(".rlg327")

for i in range(1, len(data)):
	filename = data[i-1].split('\n')[-1]+'.rlg327'	
	game_map = data[i].split('\n')[2:23]
	non_tunneling = data[i].split('\n')[23:44]
	tunneling = data[i].split('\n')[44:65]
	
	run_sync('cp ' + filename + ' ' + os.environ['HOME'] + '/.rlg327/dungeon')
	result = run_sync('../tmp/feddersen_jacob.assignment-1.03/generate_dungeon --load')
	
	map_result = result.split('\n')[0:21]
	for j in range(len(map_result)):
		map_result[j] = map_result[j].replace('-', ' ')
		map_result[j] = map_result[j].replace('|', ' ')
	non_tunneling_result = result.split('\n')[21:42]
	tunneling_result = result.split('\n')[42:63]
	
	if (game_map == map_result):
		print(filename + ' game map is correct!')
	else:
		print(filename + ' game map is not correct:')
		print('Expected:')
		print(game_map)
		print('Actual:')
		print(map_result)
		
	if (non_tunneling == non_tunneling_result):
		print(filename + ' non-tunneling map is correct!')
	else:
		print(filename + ' non-tunneling map is not correct:')
		print('Expected:')
		print(non_tunneling)
		print('Actual:')
		print(non_tunneling_result)
		
	if (tunneling == tunneling_result):
		print(filename + ' tunneling map is correct!')
	else:
		print(filename + ' tunneling map is not correct:')
		print('Expected:')
		print(tunneling)
		print('Actual:')
		print(tunneling_result)
	
	print('--------------------------------------')
