#ifndef DRAW_DUNGEON_H
#define DRAW_DUNGEON_H

#include <stdint.h>

#include "dungeon.h"

/**
 * Draw the dungeon
 */
void draw_map(dungeon_t* d);

void draw_dist_map(dungeon_t *d, uint32_t dist[MAP_HEIGHT][MAP_WIDTH], int hardness_limit);

#endif
