#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "util.h"

int randrange(int min, int max) {
	int range = max - min + 1;
	return (rand() % range) + min;
}

int randchance(double prob) {
	int resolution = RAND_MAX;
	return (rand() % resolution) < (prob * resolution);
}

int sign(int x) {
	if (x > 0) return 1;
	if (x < 0) return -1;
	return 0;
}

void parse_args(int argc, char *argv[], args_t *args) {
  int i;
  
  // Default parameter values
  args->dbg = 0;
  args->load = 0;
  args->save = 0;
  args->nummon = 5;
  args->seed = time(NULL);
	args->sleeptime = 250000;
  
  int valid = 1;
  
  for (i = 1; i < argc; i++) {
		if (!strcmp(argv[i], "--save")) {
			args->save = 1;
		}	else if (!strcmp(argv[i], "--load")) {
			args->load = 1;
		} else if (!strncmp(argv[i], "--seed=", 7)) {
      if (strlen(argv[i]) > 7) {
        args->seed = atoi(argv[i] + 7);
      } else {
        valid = 0;
      }
    } else if (!strncmp(argv[i], "--nummon=", 9)) {
      if (strlen(argv[i]) > 9) {
        args->nummon = atoi(argv[i] + 9);
      } else {
        valid = 0;
      }
    } else if (!strncmp(argv[i], "--step=", 7)) {
			if (strlen(argv[i]) > 7) {
				args->sleeptime = atoi(argv[i] + 7);
			} else {
				valid = 0;
			}
		} else if (!strcmp(argv[i], "--dbg")) {
      args->dbg = 1;
    } else if (!strcmp(argv[i], "-h")) {
      valid = 0;
    } else {
      valid = 0;
    }
  }
  
  if (!valid) {
    fprintf(stderr, "Usage: %s [--load --save --dbg --seed=<seed> --nummon=<monsters to spawn>\n\t\t\t--step=<step time in us>]\n\n", argv[0]);
    fprintf(stderr, "    --load:  \tInstead of generating a random dungeon, loads the saved dungeon\n\t\tfile from ~/.rlg327/dungeon\n");
    fprintf(stderr, "    --save:  \tSaves the dungeon to ~/.rlg327/dungeon\n");
    fprintf(stderr, "    --seed:  \tSeeds the random number generator to the given value. If not\n\t\tspecified, time(NULL) is used as the seed\n");
    fprintf(stderr, "    --nummon:\tSets the number of monsters to generate in the dungeon. If not\n\t\tspecified, the default is 5. Automatically capped at the number\n\t\tof empty spaces in the dungeon.\n\t\tIf nummon is 0, the game never ends and the PC wanders the\n\t\tdungeon endlessly.\n");
		fprintf(stderr, "    --step:  \tSets the time to sleep between each game player turn step.\n\t\tDefault is 250000 us.\n");
    fprintf(stderr, "    --dbg:   \tEnables debug output\n");
    fprintf(stderr, "    -h:      \tPrints this help information and exits\n");
    exit(1);
  }
}

void print_args(args_t *args) {
  printf("Program Arguments:\n\tLoad: %d\n\tSave: %d\n\tSeed: %d\n\tMonsters: %d\n", args->load, args->save, args->seed, args->nummon);
}
