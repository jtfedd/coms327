#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>

#include "character.h"
#include "dungeon.h"
#include "draw_dungeon.h"
#include "distance_map.h"
#include "display_result.h"
#include "line_of_sight.h"
#include "movement_logic.h"
#include "save_load.h"
#include "util.h"

int main(int argc, char* argv[]) {
  
  args_t args;
  parse_args(argc, argv, &args);
  
  if (args.dbg) {
    print_args(&args);
  }
	
	srand(args.seed);
	
	dungeon_t d;
  init_dungeon(&d);
	
	if (args.load) {
		load_dungeon(&d);
	} else {
		gen_random_dungeon(&d);
	}
  
  gen_monsters(&d, args.nummon);
	
	if (args.save) {
		save_dungeon(&d);
	}
	
	draw_map(&d);
  usleep(args.sleeptime);

  heap_t h;
  init_turn_heap(&d, &h);
  
  int game_won;
  
  // Initialize line of sight
	calc_line_of_sight(&d, d.pc_sight, d.player_pos.x, d.player_pos.y);
  
  while(1) {
    character_t *c = heap_remove_min(&h);
    
    // If the character is not alive, then free its memory
    if (!c->alive) {
      if (c->player) {
        free(c->player);
        free(c);
        game_won = 0;
        break;
      } else {
        free(c->monster);
        free(c);
        if (h.size == 1) {
          // Only one character left and the game isn't over, the player must have won
          game_won = 1;
          break;
        } else {
          continue;
        }
      }
    }
    
		// The character decides where to move next
    position_t next_move = character_move(&d, c);
        
    // If we killed another character by moving there
    if (d.characters[next_move.y][next_move.x] && d.characters[next_move.y][next_move.x] != c) {
      d.characters[next_move.y][next_move.x]->alive = 0;
    }
    
    // Check for rock and tunneling
    if (d.hardness[next_move.y][next_move.x] && d.hardness[next_move.y][next_move.x] < 255 && has_characteristic(c, TUNNEL)) {
      if (d.hardness[next_move.y][next_move.x] > 85) {
        d.hardness[next_move.y][next_move.x] -= 85;
			} else {
				d.hardness[next_move.y][next_move.x] = 0;
			}
			
			// Update the line of sight
			calc_line_of_sight(&d, d.pc_sight, d.player_pos.x, d.player_pos.y);
    }
    
    // If the space we want to move to has hardness zero, move there
    if (d.hardness[next_move.y][next_move.x] == 0) {
      d.characters[c->pos.y][c->pos.x] = NULL;
      d.characters[next_move.y][next_move.x] = c;
      c->pos.x = next_move.x;
      c->pos.y = next_move.y;
      if (c->player) {
        d.player_pos.x = next_move.x;
        d.player_pos.y = next_move.y;
      }
    }
    
    // Update the character's turn and reinsert into heap
    c->next_turn = c->next_turn + (1000 / c->speed);
    heap_insert(&h, c);
    
		// If the player moved, update line of sight, redraw, and sleep for the timestep
    if (c->player) {
			calc_line_of_sight(&d, d.pc_sight, d.player_pos.x, d.player_pos.y);
      draw_map(&d);
      usleep(args.sleeptime);
    }
  }
  
  if (game_won) {
    display_win();
  } else {
    display_lose();
  }
	
  heap_delete(&h);
	free_dungeon(&d);
}
