#ifndef MOVEMENT_LOGIC_H
#define MOVEMENT_LOGIC_H

#include "dungeon.h"
#include "character.h"
#include "util.h"

position_t character_move(dungeon_t *d, character_t *c);

#endif
