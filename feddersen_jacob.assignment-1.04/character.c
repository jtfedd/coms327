#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "character.h"
#include "heap.h"
#include "util.h"

static int32_t character_turn_cmp(const void *key, const void *with) {
  if (((character_t *) key)->next_turn == ((character_t *) with)->next_turn) {
    return ((character_t *) key)->seq_num - ((character_t *) with)->seq_num;
  } else {
    return ((character_t *) key)->next_turn - ((character_t *) with)->next_turn;
  }
}

char characteristics_symbols[] = "0123456789abcdef";

character_t *generate_monster(uint8_t x, uint8_t y) {
  static int next_seq = 1;
  uint32_t characteristics = rand() & 0x0000000f;
  
  character_t *m = malloc(sizeof(character_t));
  m->monster = malloc(sizeof(monster_t));
  m->player = NULL;
  
  m->monster->characteristics = characteristics;
  
  // Initialize the starting "last seen" position to the position of the monster
  // This will keep smart monsters from moving until they actually see the player
  m->monster->last_seen.x = x;
  m->monster->last_seen.y = y;
  
  m->pos.x = x;
  m->pos.y = y;
  m->speed = randrange(5, 20);
  m->seq_num = next_seq++;
  m->next_turn = 0;
  m->alive = 1;
  m->symbol = characteristics_symbols[characteristics];
  
  return m;
}

character_t *generate_pc(uint8_t x, uint8_t y) {
  character_t *pc = malloc(sizeof(character_t));
  
  pc->player = malloc(sizeof(pc_t));
  pc->monster = NULL;
  
  pc->player->target.x = x;
  pc->player->target.y = y;
  
  pc->pos.x = x;
  pc->pos.y = y;
  pc->speed = 10;
  pc->seq_num = 0;
  pc->next_turn = 0;
  pc->alive = 1;
  pc->symbol = '@';
  
  return pc;
}

void init_character_turn_heap(heap_t *h) {
  heap_init(h, character_turn_cmp, NULL);
}
