#include <stdio.h>
#include <stdint.h>
#include <limits.h>

#include "dungeon.h"
#include "draw_dungeon.h"

char monster_chars[] = "0123456789abcdef";

void draw_dist_map(dungeon_t *d, uint32_t dist[MAP_HEIGHT][MAP_WIDTH], int hardness_limit) {
	int x, y;
	for (y = 0; y < MAP_HEIGHT; y++) {
		for (x = 0; x < MAP_WIDTH; x++) {			
			if (y == d->player_pos.y && x == d->player_pos.x) {
				printf("@");
      } else if (d->hardness[y][x] <= hardness_limit) {
        if (dist[y][x] == INT_MAX) printf("X");
        else printf("%d", dist[y][x] % 10);
      } else {
        printf(" ");
      }
		}
		printf("\n");
	}
}

char get_map_char(dungeon_t *d, int y, int x) {
	int i;
	
	// Check each position from top down, i.e. check the layers which would
	// render over other layers first.
	
	if (y == 0 || y == (MAP_HEIGHT-1) || x == 0 || x == (MAP_WIDTH-1)) {
		if (y == 0 || y == (MAP_HEIGHT - 1)) return '-';
		else return '|';
	}
	
	if (d->characters[y][x]) {
		return d->characters[y][x]->symbol;
	}
	
	for (i = 0; i < d->upstair_count; i++) {
		if (x == d->upstair_list[i].x && y == d->upstair_list[i].y) {
			return '<';
		}
	}
	
	for (i = 0; i < d->downstair_count; i++) {
		if (x == d->downstair_list[i].x && y == d->downstair_list[i].y) {
			return '>';
		}
	}
	
	for (i = 0; i < d->room_count; i++) {
		room_t *r = d->room_list + i;
		if (x >= r->x && x < r->x + r->w && y >= r->y && y < r->y + r->h) {
			return '.';
		}
	}
	
	if (d->hardness[y][x] == 0) {
		return '#';
	}
	
	return ' ';
}

void draw_map(dungeon_t *d) {
	int i, j;
	for (i = 0; i < MAP_HEIGHT; i++) {
		for (j = 0; j < MAP_WIDTH; j++) {
			printf("%c", get_map_char(d, i, j));
		}
		printf("\n");
	}
}
