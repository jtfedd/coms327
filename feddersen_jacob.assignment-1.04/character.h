#ifndef CHARACTER_H
#define CHARACTER_H

#include <stdint.h>

#include "heap.h"
#include "util.h"

#define NPC_SMART   0x00000001
#define NPC_TELE    0x00000002
#define NPC_TUNNEL  0x00000004
#define NPC_ERRATIC 0x00000008

#define has_characteristic(c, bit)	\
	((c->player) ? 0 : (c->monster->characteristics) & NPC_##bit)

typedef struct player_character {
  position_t target;
} pc_t;

typedef struct monster {
  uint32_t characteristics;
	position_t last_seen;
} monster_t;

typedef struct character {
  pc_t *player;
  monster_t *monster;
  position_t pos;
  int speed;
  int seq_num;
  int next_turn;
  int alive;
  char symbol;
} character_t;

character_t *generate_monster(uint8_t x, uint8_t y);

character_t *generate_pc(uint8_t x, uint8_t y);

void init_character_turn_heap(heap_t *h);

#endif
