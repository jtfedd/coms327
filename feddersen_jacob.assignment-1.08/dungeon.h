#ifndef DUNGEON_H
#define DUNGEON_H

#include <cstdint>
#include <vector>

#include "character.h"
#include "descriptions.h"
#include "util.h"

#define MAP_WIDTH 80
#define MAP_HEIGHT 21

#define MAX_ROOMS 12
#define MIN_ROOMS 6

#define ITEMS_PER_LEVEL 15

class character;

class room {
 public:
  uint8_t x;
  uint8_t y;
  uint8_t w;
  uint8_t h;
  
  room();
};

class dungeon {
 public:
  uint8_t hardness[MAP_HEIGHT][MAP_WIDTH];
  bool hardness_dirty;
  
  std::vector<monster_description> monster_descriptions;
  std::vector<object_description> object_descriptions;
  
  uint8_t pc_sight[MAP_HEIGHT][MAP_WIDTH];
  
  char terrain[MAP_HEIGHT][MAP_WIDTH];
  char remembered_terrain[MAP_HEIGHT][MAP_WIDTH];
  
  uint16_t room_count;
  room* room_list;
  
  uint16_t upstair_count;
  position* upstair_list;
  
  uint16_t downstair_count;
  position* downstair_list;
  
  position player_pos;
  
  character *characters[MAP_HEIGHT][MAP_WIDTH];
  std::vector<item *> items[MAP_HEIGHT][MAP_WIDTH];

 private:
  bool fog_of_war;
  uint8_t fow_map[MAP_HEIGHT][MAP_WIDTH];
  
  bool target_mode;
  position targeting_pointer;
  
  void generate_rooms();
  void generate_corridors();
  void connect_rooms(int room1, int room2);
  void init_dungeon_hardness();
  void generate_stairs();
  
  void draw_map_char(int y, int x);
  
 public:
  dungeon();
  ~dungeon();
  
  void gen_random_dungeon();
  void init_turn_heap(heap_t *h);
  void gen_monsters(int nummon);
  void gen_items(int numitems);
  
  void draw();
  
  void free_data();
  
  void toggle_fog_of_war();
  void update_fog_of_war();
  
  int targeting_mode();
  int display_monster_list();
};

#endif
