#ifndef LINE_OF_SIGHT_H
#define LINE_OF_SIGHT_H

#include "dungeon.h"

#define VIEW_DIST 2

void calc_line_of_sight(dungeon &d);

#endif
