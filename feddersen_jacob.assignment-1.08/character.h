#ifndef CHARACTER_H
#define CHARACTER_H

#include <cstdint>

#include "dice.h"
#include "heap.h"
#include "util.h"

#define NPC_SMART   0x00000001
#define NPC_TELE    0x00000002
#define NPC_TUNNEL  0x00000004
#define NPC_ERRATIC 0x00000008
#define NPC_BOSS		0x00000010
#define NPC_UNIQ		0x00000020
#define NPC_DESTROY_OBJ 0x00000040
#define NPC_PICKUP_OBJ  0x00000080
#define NPC_PASS_WALL   0x00000100

class dungeon;
class monster_description;
class object_description;

class character {
 public:
  position pos;
  int speed;
  int seq_num;
  int next_turn;
  int alive;
  char symbol;
  int hitpoints;
	
	character(int speed, char symbol, int hitpoints);
	virtual ~character();
	
	virtual bool is_player() = 0;
	virtual int has_characteristic(int bit) = 0;
  virtual int next_color() = 0;
};

class player_character : public character {
 public:
	player_character(uint8_t x, uint8_t y);
	
  bool is_player();
	int has_characteristic(int bit);
  
  int next_color();
};

class monster : public character {
 private:
  monster_description *desc;
  dice *damage;
	uint32_t characteristics;
	position last_seen;
  int color_index;
	
	position next_monster_move_tunnel_erratic(dungeon &d);
	position next_monster_move_erratic(dungeon &d);
	position next_monster_move_smart(dungeon &d);
	position next_monster_move_dumb(dungeon &d);
	
 public:
	monster(uint8_t x, uint8_t y, monster_description *desc, int speed, uint32_t abilities, int hitpoints, dice *damage, char symbol);
	~monster();
  
	bool is_player();
	int has_characteristic(int bit);
	
	position monster_move(dungeon &d);
	void init_position(uint8_t x, uint8_t y);
  
  int next_color();
  
  void die();
};

void init_character_turn_heap(heap_t *h);

class item {
 public:
  object_description *desc;
  dice *damage;
  int hit;
  int dodge;
  int defence;
  int weight;
  int speed;
  int attribute;
  int value;
  
  item(object_description *desc, dice *damage, int hit, int dodge, int defence, int weight, int speed, int attribute, int value);
  ~item();
  
  int get_color();
  char get_symbol();
};

#endif
