==7914== Memcheck, a memory error detector
==7914== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==7914== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==7914== Command: ./rlg327
==7914== Parent PID: 2447
==7914== 
==7914== 
==7914== HEAP SUMMARY:
==7914==     in use at exit: 76,570 bytes in 333 blocks
==7914==   total heap usage: 47,529 allocs, 47,196 frees, 2,483,347 bytes allocated
==7914== 
==7914== LEAK SUMMARY:
==7914==    definitely lost: 0 bytes in 0 blocks
==7914==    indirectly lost: 0 bytes in 0 blocks
==7914==      possibly lost: 0 bytes in 0 blocks
==7914==    still reachable: 76,570 bytes in 333 blocks
==7914==         suppressed: 0 bytes in 0 blocks
==7914== Rerun with --leak-check=full to see details of leaked memory
==7914== 
==7914== For counts of detected and suppressed errors, rerun with: -v
==7914== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
