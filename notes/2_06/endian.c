#include <stdio.h>
#include <stdlib.h>
#include <endian.h>
#include <time.h>
#include <stdint.h>

int main() {
	srand(time(NULL));
	uint32_t random = rand();

	printf("%u\n", random);
	printf("%u\n", htobe32(random));
	printf("%u\n", be32toh(htobe32(random)));
}
