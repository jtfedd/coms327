#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
	char *home = getenv("HOME");
	char *path;

	path = malloc(strlen(home) + strlen("/.rlg327/dungeon") + 1);

	strcpy(path, home);
	strcat(path, "/.rlg327/dungeon");

	printf("%s\n", path);
}
