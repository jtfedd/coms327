#define _GNU_SOURCE
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

char* MYSTR = "This is a fun string to fry!";

int main(int argc, char* argv[]) {
	strfry(MYSTR);
	printf("%s\n", MYSTR);
}
