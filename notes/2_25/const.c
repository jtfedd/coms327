#include <stdio.h>

int main(int argc, char *argv[]) {
	const int i = 0;

	// Let's try to reassign i!

	// Generates error
	//i = 1;

	// Error: lvalue required as left operand of assignment
	//(int) i = 1;

	// Works, but generates warning
	//int *p = &i;
	//*p = 1;

	// Works
	*(int *) &i = 1;

	printf("%d\n", i);

	return 0;
}
