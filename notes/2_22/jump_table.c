#include <stdio.h>

// Type func: pointer to function that returns void and takes void
typedef void (*func)(void);

void zero()
{
	printf(__FUNCTION__);
}

void one()
{
	printf(__FUNCTION__);
}

void two()
{
	printf(__FUNCTION__);
}

void three()
{
	printf(__FUNCTION__);
}

void four()
{
	printf(__FUNCTION__);
}

int main(int argc, char *argv[]) {
	func table[] = {
		zero,
		one,
		two,
		three,
		four
	};

	int i;
	for( i = 0; i < 5; i++) {
		table[i]();
	}

	return 0;
}
