#include <iostream>

using namespace std;

class A {
public:
	A() { cout << "Constructing an A" << endl; }
	A(const A &a) { cout << "Copying an A" << endl; }
	virtual ~A() { cout << "Destroying an A" << endl; }
	virtual void print() { cout << "A" << endl; }
};

class B : virtual public A {
public:
	B() { cout << "Constructing a B" << endl; }
	B(const B &b) { cout << "Copying a B" << endl; }
	virtual ~B() { cout << "Destroying a B" << endl; }
	virtual void print() { cout << "B" << endl; }
};

class C : virtual public A {
public:
	C() { cout << "Constructing a C" << endl; }
	C(const C &c) { cout << "Copying a C" << endl; }
	virtual ~C() { cout << "Destroying a C" << endl; }
	virtual void print() { cout << "C" << endl; }
};

class D : virtual public B, virtual public C {
public:
	D() { cout << "Constructing a D" << endl; }
	D(const D &d) { cout << "Copying a D" << endl; }
	virtual ~D() { cout << "Destroying a D" << endl; }
	virtual void print() { cout << "D" << endl; }
};

int main(int argc, char *argv[]) {
	/*
	A a;
	B b;
	C c;
	*/

	D d;
	d.print();
	d.C::print();
	d.B::print();
	((B) d).A::print();

	return 0;
}
