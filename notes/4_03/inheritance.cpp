#include <iostream>
#include <cstdlib>

using namespace std;

/*
Polymorphism in c++

Only works with references and pointers!
Cannot get polymorphic behavior with instances
I.E. Impossible to assign an instance of subclass to an instance of type superclass
*/

class shape {
public:
	int color;
	virtual double area() = 0;
	virtual double perimeter() = 0;
	virtual ~shape() {};

	friend ostream &operator<<(ostream &o, const shape &s);
	virtual ostream &print(ostream &o) const = 0;
};

ostream &operator<<(ostream &o, const shape &s) {
	return s.print(o);
}

class rectangle : public shape {
protected:
	double width, height;
public:
	double area() {
		return width * height;
	}
	double perimeter() {
		return 2 *(width + height);
	}
	rectangle() {
		width = 1;
		height = 2;
	}
	rectangle(double width, double height) {
		this->width = width;
		this->height = height;
	}
	ostream &print(ostream &o) const {
		return o << "Rectangle: Width " << width << ", Height " << height << endl;
	}
};

/*
class square : public shape {
private:
	double side;
public:
	double area() {
		return side * side;
	}
	double perimeter() {
		return 4 * side;
	}
	square() {
		side = 1;
	}
	square(double side) {
		this->side = side;
	}
	ostream &print(ostream &o) const {
		return o << "Square: Side " << side << endl;
	}
};
*/

class square : public rectangle {
private:
public:
	square() : rectangle(1, 1) { }
	square(double side) : rectangle(side, side) { }
	ostream &print(ostream &o) const {
		return o << "Square: Side " << width << endl;
	}
};

int main(int argc, char *argv[]) {
	square s(4);
	rectangle r(3, 5);
	shape *sp = &s;
	shape &sr = r;

	shape *a[10];

	cout << s << r << endl;

	cout << sp->area() << " " << sp->perimeter() << " "
		<< sr.area() << " " << sr.perimeter() << endl;

	for (int i = 0; i < 10; i++) {
		if (rand() % 2) {
			a[i] = new square(1 + rand() % 10);
		} else {
			a[i] = new rectangle(1 + rand() % 10, 1 + rand() % 10);
		}
	}

	for (int i = 0; i < 10; i++) {
		cout << *a[i];
	}

	return 0;
}
