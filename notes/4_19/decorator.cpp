#include <iostream>

using namespace std;

class shape {
public:
	virtual ~shape() {}
	virtual void draw() = 0;
};

class square : public shape {
public:
	void draw()
	{
		cout << "[]" << endl;
	}
};

class circle : public shape {
public:
	void draw()
	{
		cout << "()" << endl;
	}
};

// What if we want to add colors?

// We could extend square for every color,
// or attribute, but that doesn't scale well
class green_square : public square {
public:
	void draw()
	{
		cout << "green-[]" << endl;
	}
};

// Better way: Decorator pattern
class shape_decorator : public shape {
private:
	shape *s;
public:
	shape_decorator(shape *s) : s(s) {}
	~shape_decorator() { delete s; }
	void draw() { s->draw(); }
};

class green_shape : public shape_decorator {
public:
	green_shape(shape *s) : shape_decorator(s) {}
	void draw() {
		cout << "green-";
		shape_decorator::draw();
	}
};
class big_shape : public shape_decorator {
public:
	big_shape(shape *s) : shape_decorator(s) {}
	void draw() {
		cout << "BIG-";
		shape_decorator::draw();
	}
};

class cute_shape : public shape_decorator {
public:
	cute_shape(shape *s) : shape_decorator(s) {}
	void draw() {
		cout << "cute-";
		shape_decorator::draw();
	}
};

int main(int argc, char *argv[]) {
	square s;
	circle c;
	green_square gs;

	s.draw();
	c.draw();
	gs.draw();

	green_shape d(new square);
	d.draw();

	big_shape bs(new big_shape(new circle));
	bs.draw();
	return 0;
}
