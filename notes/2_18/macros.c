//#include <stdio.h>

#define MAX_ROOMS 20
#define min(x, y) (x < y ? x : y)

#define max(x, y) ({ \
	typeof (x) _x = x; \
	typeof (y) _y = y; \
	_x > _y ? _x : _y; \
})

int main(int argc, char *argv[]) {
	MAX_ROOMS;

	min(3, 4);
	min(foo(x), bar(x));
	max(foo(x), bar(x));

	if (10 > min(foo(x), bar(x))) {
	}

	return 0;
}
