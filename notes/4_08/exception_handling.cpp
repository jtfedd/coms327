#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {
	// Anything can be thrown as an exception

	try {
		throw "Error";
	} catch (char const *s) {
		cout << s << endl;
	}

	cout << "Hello, World!" << endl;

	return 0;
}
