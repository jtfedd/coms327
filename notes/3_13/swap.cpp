#include <iostream>

using namespace std;

void cswap(int *a, int *b) {
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

void cppswap(int &a, int &b) {
	int tmp = a;
	a = b;
	b = tmp;
}

int main(int argc, char *argv[]) {
	int i;
	int &r = i;

	r = 0;
	cout << i << endl;
	i = 10;
	cout << r << endl;

	int a = 10, b = 20;

	cswap(&a, &b);

	printf("a: %d, b: %d\n", a, b);

	cppswap(a, b);

	printf("a: %d, b: %d\n", a, b);

	return 0;
}
