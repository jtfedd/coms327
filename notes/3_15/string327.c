#include <cstring>

#include "string327.h"

string327::string327() {
	str = (char *) malloc(1);
	str[0] = '\0';
}

string327::string327(const char *s) {
	str = strdup(s);
}

string327::~string327() {
	free(str);
}

int string327::length() {
	return strlen(str);
}

const char *string327::c_str() {
	return str;
}

int string327::operator<(const string327 &s) {
	return strcmp(str, s.str) < 0;
}

int string327::operator>(const string327 &s) {
	return strcmp(str, s.str) > 0;
}

int string327::operator==(const string327 &s) {
	return strcmp(str, s.str) == 0;
}

int string327::operator!=(const string327 &s) {
	return strcmp(str, s.str) != 0;
}

int string327::operator<=(const string327 &s) {
	return strcmp(str, s.str) <= 0;
}

int string327::operator>=(const string327 &s) {
	return strcmp(str, s.str) >= 0;
}

string327 string327::operator+(const string327 &s) {
	string327 out;
	free(out.str);
	
	out.str = malloc(strlen(str) + strlen(s.str) + 1);
	strcpy(out.str, str);
	strcat(out.str, s.str);
	
	return out;
}

string327 &string327::operator+=(const string327 &s) {
	str = (char *) realloc(str, strlen(str) + strlen(s.str) + 1);
	strcat(str, s.str);
	
	return *this;
}

string327 &string327::operator=(const string327 &s) {
	free(str);
	str = strdup(s.str);
	
	return *this;
}

string327 &string327::operator=(const char *s) {
	free(str);
	str = strcup(s);
}

char &string327::operator[](const int i) {
	return str[i];
}
