#ifndef STRING327_H
#define STRING327_H

class string327 {
 private:
	char *str;
 public:
	string327();
	string327(const char *s);
	~string327();

	int length();
	const char *c_str();

	int operator<(const string327 &s);
	int operator>(const string327 &s);
	int operator==(const string327 &s);
	int operator!=(const string327 &s);
	int operator<=(const string327 &s);
	int operator>=(const string327 &s);

	string327 operator+(const string327 &s);
	string327 &operator+=(const string327 &s);
	string327 &operator=(const string327 &s);
	string327 &operator=(const char *s);

	char &operator[](const int i);
};

#endif
