#include <iostream>

using namespace std;

template <class T>
class queue {
private:
	class queue_node {
	public:
		queue_node(T t) : next(0), prev(0), data(t) { }
		queue_node(T t, queue_node *prev, queue_node *next) : next(next), prev(prev), data(t) { }
		queue_node *next, *prev;
		T data;
	};
	queue_node *head, *tail;
	int size;

	queue() : head(0), tail(0), size(0) { }
	~queue() {
		// Implement later
	}

	void enqueue(T t) {
		// Put something in
		queue_node *tmp = new queue_node(t, tail, 0);
		if (tail) {
			tail->next = tmp;
		}

		tail = tmp;
		if (!head) {
			head = tmp;
		}
	}

	T dequeue() {
		// Take something out and return it
	}

	T front() {
		// Pop the front of the queue and return it
	}

	int size() {
		return size;
	}
};
