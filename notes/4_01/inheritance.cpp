#include <iostream>

using namespace std;

class shape {
public:
	int color;
	virtual double area() = 0;
	virtual double perimeter() = 0;
	virtual ~shape() {};
};

class rectangle : public shape {
private:
	double width, height;
public:
	double area() {
		return width * height;
	}
	double perimeter() {
		return 2 *(width + height);
	}
	rectangle() {
		width = 1;
		height = 2;
	}
	rectangle(double width, double height) {
		this->width = width;
		this->height = height;
	}
	friend ostream &operator<<(ostream &o, const rectangle &r);
};

ostream &operator<<(ostream &o, const rectangle &r) {
	return o << "Rectangle: Width " << r.width << ", Height " << r.height << endl;
}

class square : public shape {
private:
	double side;
public:
	double area() {
		return side * side;
	}
	double perimeter() {
		return 4 * side;
	}
	square() {
		side = 1;
	}
	square(double side) {
		this->side = side;
	}
	friend ostream &operator<<(ostream &o, const square &s);
};

ostream &operator<<(ostream &o, const square &s) {
	return o << "Square: Side " << s.side << endl;
}

int main(int argc, char *argv[]) {
	square s(4);
	rectangle r(3, 5);
	shape *sp = &s;
	shape &sr = r;

	cout << s << s.perimeter() << endl << s.area() << endl;
	cout << r << r.perimeter() << endl << r.area() << endl;
	cout << *sp;
	cout << sr;

	for (int i = 0; i < 10; i++) {
		
	}

	return 0;
}
