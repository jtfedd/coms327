#include <stdlib.h>

#include "stack.h"

int stack_init(stack_t *s) {
	s->size = 0;
	s->top = NULL;
	return 0;
}

int stack_delete(stack_t *s) {
	stack_node_t *node;
	
	s->size = 0;
	
	for (node = s->top; node; node = s->top) {
		s->top = node->next;
		//free(node);
	}
	
	return 0;
}

int stack_peek(stack_t *s, int *value) {
	if (s->top) {
		*value = s->top->value;
		return 0;
	}
	
	return 1;
}

int stack_push(stack_t *s, int value) {
	stack_node_t* node;
	
	if (!(node = malloc(sizeof(*node)))) {
		return 1;
	}
	
	node->value = value;
	node->next = s->top;
	s->top = node;
	
	s->size++;
	
	return 0;
}

int stack_pop(stack_t *s, int *value) {
	stack_node_t *node;
	
	if (!s->top) {
		return 1;
	}
	
	node = s->top;
	s->top = node->next;
	*value = node->value;
	s->size--;
	//free(node);
	
	return 0;
}

int stack_size(stack_t *s) {
	return s->size;
}

int stack_is_empty(stack_t *s) {
	return !s->top;
}
