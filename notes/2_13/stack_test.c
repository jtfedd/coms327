#include <stdio.h>

#include "stack.h"

int main(int argc, char *argv[]) {
	int i;
	
	// Create a stack instance in memory
	// Not creating a pointer because
	// then we would have to allocate memory dynamically
	// This allocates memory on the stack
	stack_t s;
	
	// Initialize the stack
	stack_init(&s);
	
	// Push some numbers onto the stack
	for (i = 0; i < 5; i++) {
		stack_push(&s, i);
	}
	
	// Peek at the top value; get the value by reference
	stack_peek(&s, &i);
	printf("%d\n", i);
	
	// Pop all the values off the top of the stack
	while(!stack_peek(&s, &i)) {
		printf("%d\n", i);
		stack_pop(&s, &i);
	}
	
	// Verify the stack size
	printf("%d\n", stack_size(&s));
	
	// Fill the stack back up and try to delete it
	for (i = 0; i < 5; i++) {
		stack_push(&s, i);
	}
	stack_delete(&s);
	
	// Check the size again
	printf("%d\n", stack_size(&s));
	
	return 0;
}