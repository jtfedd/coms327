//#include <stdio.h>

#define NPC_SMART   0x00000001
#define NPC_TELE    0x00000002
#define NPC_TUNNEL  0x00000004
#define NPC_ERRATIC 0x00000008

#define MAX_ROOMS 20
#define NOTHING

#define to_string(s) #s

#define table_element(e) { e, #e }
struct {int, char * } table[] = {
	table_element(i);
	table_element(j);
};

#define has_characteristic(character, bit)		\
	((character) & NPC_##bit)

#ifdef NOTHING
int Idontmatter = MAX_ROOMS;
#endif
// Not the best, evalues each argument twice
#define min(x, y) (x < y ? x : y)

int characteristics = rand() % 16;

// Better, evaluates each argument once and uses those results
#define max(x, y) ({ \
	typeof (x) _x = x; \
	typeof (y) _y = y; \
	_x > _y ? _x : _y; \
})

int main(int argc, char *argv[]) {
	MAX_ROOMS;

	min(3, 4);
#if 0
	min(foo(x), bar(x));
	max(foo(x), bar(x));

	if (10 > min(foo(x), bar(x))) {
	}
#endif
	int x = NOTHING;

	if(has_characteristic(characteristic, SMART)) {
		// This is a smart monster
	} else {
		// This is a not smart monster
	}

	to_string(Foo);

	return 0;
}
