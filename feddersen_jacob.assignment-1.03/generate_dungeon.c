#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <stdint.h>
#include <limits.h>

#include "heap.h"

#define MAP_WIDTH 80
#define MAP_HEIGHT 21

#define MAX_ROOMS 12
#define MIN_ROOMS 6

#define FILETYPE_MARKER "RLG327-S2019"
#define VERSION 0

#define FILENAME "/.rlg327/dungeon"

typedef struct dijkstra_path {
  heap_node_t *hn;
  uint8_t pos[2];
  int32_t cost;
} dijkstra_path_t;

static int32_t dijkstra_path_cmp(const void *key, const void *with) {
  return ((dijkstra_path_t *) key)->cost - ((dijkstra_path_t *) with)->cost;
}

typedef struct {
	uint8_t x;
	uint8_t y;
	uint8_t w;
	uint8_t h;
} room;

typedef struct {
	uint8_t x;
	uint8_t y;
} position;

typedef struct {
	uint8_t hardness[MAP_HEIGHT][MAP_WIDTH];
	
	uint16_t room_count;
	room* room_list;
	
	uint16_t upstair_count;
	position* upstair_list;
	
	uint16_t downstair_count;
	position* downstair_list;
	
	position player_pos;
} dungeon;

/**
 * Generate a random number between min and max, inclusive
**/
int randrange(int min, int max);

/**
 * Probability generator
 */
int randchance(double prob);

/**
 * Returns the sign of a number
 * Or zero if the input is zero
 */
int sign(int x);

/**
 * Draw the dungeon
 */
void draw_map(dungeon* d);

/**
 * Generate random rooms in the dungeon, and place them in the provided array.
 * Return the number of rooms actually generated
 */
void generate_rooms(dungeon* d);

/**
 * Generate corridors to connect the rooms
 */
void generate_corridors(dungeon* d);

/**
 * Connect the two given rooms by a corridor
 */
void connect_rooms(dungeon* d, int room1, int room2);

/**
 * Generate stairs in random positions on rooms or corridors
 */
void generate_stairs(dungeon* d);

/**
 * Frees all of the malloc'd pointers in the dungeon struct
 */
void free_dungeon(dungeon* d);

/**
 * Initialized the dungeon hardness array
 */
void init_dungeon_hardness(dungeon* d);

/**
 * Generate a random dungeon
 */
void gen_random_dungeon(dungeon* d);

/**
 * Load the dungeon from ~/.rlg327/dungeon
 */
void load_dungeon(dungeon* d);

/**
 * Save the dungeon to ~/.rlg327/dungeon
 */
void save_dungeon(dungeon* d);

/**
 * Opens the save file ~/.rlg327/dungeon
 */
FILE* open_save_file(char* mode);

/**
 * Helper functions for load/save with endianness
 */
void read_short(FILE* f, uint16_t* val);
void read_int(FILE* f, uint32_t* val);
void write_short(FILE* f, uint16_t val);
void write_int(FILE* f, uint32_t val);

/**
 * Dijkstra's Algorithm - Find the shortest paths in the dungeon to the player position
 * hardness_limit - the smallest hardness value that the monsters cannot pass through
 *
 * Algorithm is modified from Dr. Sheaffer's Dijkstra code using his provided heap.
 */
static void dijkstra_from_player(dungeon *d, uint32_t dist[MAP_HEIGHT][MAP_WIDTH], int hardness_limit);

void draw_dist_map(dungeon *d, uint32_t dist[MAP_HEIGHT][MAP_WIDTH], int hardness_limit);

int main(int argc, char* argv[]) {
	int i;
	
	int load = 0;
	int save = 0;
	
	for (i = 1; i < argc; i++) {
		if (!strcmp(argv[i], "--save")) {
			save = 1;
		}
		
		if (!strcmp(argv[i], "--load")) {
			load = 1;
		}
	}
	
	srand(time(NULL));
	
	dungeon d;
	
	if (load) {
		load_dungeon(&d);
	} else {
		gen_random_dungeon(&d);
	}
	
	if (save) {
		save_dungeon(&d);
	}
	
	draw_map(&d);
	
	uint32_t non_tunneling_dist[MAP_HEIGHT][MAP_WIDTH];
	uint32_t tunneling_dist[MAP_HEIGHT][MAP_WIDTH];

	dijkstra_from_player(&d, non_tunneling_dist, 1);
	draw_dist_map(&d, non_tunneling_dist, 0);
	
	dijkstra_from_player(&d, tunneling_dist, 255);
	draw_dist_map(&d, tunneling_dist, 255);
	
	free_dungeon(&d);
}

static void dijkstra_from_player(dungeon *d, uint32_t dist[MAP_HEIGHT][MAP_WIDTH], int hardness_limit)
{
	// Initialize the vairables
	static dijkstra_path_t path[MAP_HEIGHT][MAP_WIDTH], *p;
	static uint32_t initialized = 0;
	heap_t h;
	uint32_t x, y;

	// Initialize the positions of each path node
	if (!initialized) {
		for (y = 0; y < MAP_HEIGHT; y++) {
			for (x = 0; x < MAP_WIDTH; x++) {
				path[y][x].pos[0] = y;
				path[y][x].pos[1] = x;
			}
		}
		initialized = 1;
	}

	// Initialize the path cost for each node
	for (y = 0; y < MAP_HEIGHT; y++) {
		for (x = 0; x < MAP_WIDTH; x++) {
			path[y][x].cost = INT_MAX;
		}
	}
	
	// The player position gets cost 0
	path[d->player_pos.y][d->player_pos.x].cost = 0;

	// Initialize the heap
	heap_init(&h, dijkstra_path_cmp, NULL);

	// Initialize all of the heap pointers
	// Also initialize the distance map
	for (y = 0; y < MAP_HEIGHT; y++) {
		for (x = 0; x < MAP_WIDTH; x++) {
      dist[y][x] = INT_MAX;
			if (d->hardness[y][x] < hardness_limit) {
				path[y][x].hn = heap_insert(&h, &path[y][x]);
			} else {
				path[y][x].hn = NULL;
			}
		}
	}

	// Main Dijkstra's Algorithm implementation
	while ((p = heap_remove_min(&h))) {
		// Visit the current node, and set its distance
		p->hn = NULL;
		dist[p->pos[0]][p->pos[1]] = p->cost;
    
    // If this node is at infinity distance, don't visit its neighbors; it is disconnected
    if (p->cost == INT_MAX) {
      continue;
    }
		
		// Iterate through all neighbors
		for (y = p->pos[0] - 1; y < p->pos[0] + 2; y++) {
			for (x = p->pos[1] - 1; x < p->pos[1] + 2; x++) {
				if (y == p->pos[0] && x == p->pos[1]) continue;
				
				if ((path[y][x].hn) && (path[y][x].cost > p->cost + 1 + (d->hardness[p->pos[0]][p->pos[1]] / 85))) {
					path[y][x].cost = p->cost + 1 + (d->hardness[p->pos[0]][p->pos[1]] / 85);

					heap_decrease_key_no_replace(&h, path[y][x].hn);
				}
			}
		}
	}
}

void draw_dist_map(dungeon *d, uint32_t dist[MAP_HEIGHT][MAP_WIDTH], int hardness_limit) {
	int x, y;
	for (y = 0; y < MAP_HEIGHT; y++) {
		for (x = 0; x < MAP_WIDTH; x++) {			
			if (y == d->player_pos.y && x == d->player_pos.x) {
				printf("@");
      } else if (d->hardness[y][x] <= hardness_limit) {
        if (dist[y][x] == INT_MAX) printf("X");
        else printf("%d", dist[y][x] % 10);
      } else {
        printf(" ");
      }
		}
		printf("\n");
	}
}

FILE* open_save_file(char* mode) {
	char *home = getenv("HOME");
	char *path;

	path = malloc(strlen(home) + strlen(FILENAME) + 1);

	strcpy(path, home);
	strcat(path, FILENAME);
	
	FILE* f = fopen(path, mode);
	
	if (!f) {
		fprintf(stderr, "Error: Cannot open file: %s\n", path);
		free(path);
		exit(-1);
	}
	
	free(path);
	
	return f;
}

void read_short(FILE* f, uint16_t* val) {
	uint16_t tmp;
	fread(&tmp, 2, 1, f);
	*val = be16toh(tmp);
}

void read_int(FILE* f, uint32_t* val) {
	uint32_t tmp;
	fread(&tmp, 4, 1, f);
	*val = be32toh(tmp);
}

void write_short(FILE* f, uint16_t val) {
	uint16_t tmp = val;
	tmp = htobe16(tmp);
	fwrite(&tmp, 2, 1, f);
}

void write_int(FILE* f, uint32_t val) {
	uint32_t tmp = val;
	tmp = htobe32(tmp);
	fwrite(&tmp, 4, 1, f);
}

void load_dungeon(dungeon* d) {
	int i;
	
	FILE* f = open_save_file("r");
	
	// Skip the file type marker, version, and filesize
	fseek(f, 20, SEEK_CUR);
	
	// Read the player position
	fread(&(d->player_pos.x), 1, 1, f);
	fread(&(d->player_pos.y), 1, 1, f);
	
	// Read the hardness map
	fread(d->hardness, 1, MAP_WIDTH * MAP_HEIGHT, f);
	
	// Read the rooms
	read_short(f, &(d->room_count));
	d->room_list = malloc(d->room_count * sizeof(room));
	
	for (i = 0; i < d->room_count; i++) {
		fread(&(d->room_list[i].x), 1, 1, f);
		fread(&(d->room_list[i].y), 1, 1, f);
		fread(&(d->room_list[i].w), 1, 1, f);
		fread(&(d->room_list[i].h), 1, 1, f);
	}
	
	// Read the up staircases
	read_short(f, &(d->upstair_count));
	d->upstair_list = malloc(d->upstair_count * sizeof(position));
	
	for (i = 0; i < d->upstair_count; i++) {
		fread(&(d->upstair_list[i].x), 1, 1, f);
		fread(&(d->upstair_list[i].y), 1, 1, f);
	}
	
	// Read the down staircases
	read_short(f, &(d->downstair_count));
	d->downstair_list = malloc(d->downstair_count * sizeof(position));
	
	for (i = 0; i < d->downstair_count; i++) {
		fread(&(d->downstair_list[i].x), 1, 1, f);
		fread(&(d->downstair_list[i].y), 1, 1, f);
	}
	
	fclose(f);
}

void save_dungeon(dungeon* d) {
	int i;
	
	FILE* f = open_save_file("w");
	
	// Write the file-type maker
	fprintf(f, "%s", FILETYPE_MARKER);
	
	// Write the version number
	write_int(f, VERSION);
	
	// Write the filesize
	write_int(f, 1708 + (d->room_count*4) + (d->downstair_count*2) + (d->upstair_count*2));
	
	// Write the player position
	fwrite(&(d->player_pos.x), 1, 1, f);
	fwrite(&(d->player_pos.y), 1, 1, f);
	
	// Write the hardness map
	fwrite(d->hardness, 1, MAP_WIDTH * MAP_HEIGHT, f);
	
	// Write the rooms
	write_short(f, d->room_count);
	
	for (i = 0; i < d->room_count; i++) {
		fwrite(&(d->room_list[i].x), 1, 1, f);
		fwrite(&(d->room_list[i].y), 1, 1, f);
		fwrite(&(d->room_list[i].w), 1, 1, f);
		fwrite(&(d->room_list[i].h), 1, 1, f);
	}
	
	// Write the up staircases
	write_short(f, d->upstair_count);
	
	for (i = 0; i < d->upstair_count; i++) {
		fwrite(&(d->upstair_list[i].x), 1, 1, f);
		fwrite(&(d->upstair_list[i].y), 1, 1, f);
	}
	
	// Read the down staircases
	write_short(f, d->downstair_count);

	for (i = 0; i < d->downstair_count; i++) {
		fwrite(&(d->downstair_list[i].x), 1, 1, f);
		fwrite(&(d->downstair_list[i].y), 1, 1, f);
	}
	
	fclose(f);
}

void gen_random_dungeon(dungeon* d) {
	init_dungeon_hardness(d);
	
	// Generate rooms
	generate_rooms(d);
	
	// Generate corridors
	generate_corridors(d);
	
	// Generate stairs
	generate_stairs(d);
	
	// Generate player position
	
	// Pick a random room
	int r = randrange(0, d->room_count-1);
	d->player_pos.x = randrange(d->room_list[r].x, d->room_list[r].x + d->room_list[r].w - 1);
	d->player_pos.y = randrange(d->room_list[r].y, d->room_list[r].y + d->room_list[r].h - 1);
}

void free_dungeon(dungeon* d) {
	free(d->room_list);
	free(d->upstair_list);
	free(d->downstair_list);
}

void init_dungeon_hardness(dungeon* d) {
	int i, j;
	for (i = 0; i < MAP_HEIGHT; i++) {
		for (j = 0; j < MAP_WIDTH; j++) {
			if (i == 0 || i == (MAP_HEIGHT - 1) || j == 0 || j == (MAP_WIDTH - 1)) {
				d->hardness[i][j] = 255;
			} else {
				d->hardness[i][j] = randrange(1, 254);
			}
		}
	}
}

void generate_stairs(dungeon* d) {
	int found_pos, i, j;
	
	// Up stairs
	int num_up_stairs = randrange(1, 2);
	d->upstair_count = num_up_stairs;
	d->upstair_list = malloc(num_up_stairs * sizeof(position));
	
	for (i = 0; i < num_up_stairs; i++) {
		found_pos = 0;
		while (!found_pos) {
			int x = randrange(1, MAP_WIDTH-2);
			int y = randrange(1, MAP_HEIGHT-2);

			if (d->hardness[y][x] == 0) {
				// This is an open space, let's see if it is already a stair
				int valid = 1;
				for (j = 0; j < i; j++) {
					if (d->upstair_list[i].x == x && d->upstair_list[i].y == y) {
						valid = 0;
					}
				}
				// If not valid, keep trying
				if (!valid) continue;
				
				d->upstair_list[i].x = x;
				d->upstair_list[i].y = y;
				found_pos = 1;
			}
		}
	}
		
	// Down stairs
	int num_down_stairs = randrange(1, 2);
	d->downstair_count = num_down_stairs;
	d->downstair_list = malloc(num_down_stairs * sizeof(position));
	
	for (i = 0; i < num_down_stairs; i++) {
		found_pos = 0;
		while (!found_pos) {
			int x = randrange(1, MAP_WIDTH-2);
			int y = randrange(1, MAP_HEIGHT-2);

			if (d->hardness[y][x] == 0) {
				// This is an open space, let's see if it is already a stair
				// Have to check both up stairs and down stairs now
				int valid = 1;
				for (j = 0; j < d->upstair_count; j++) {
					if (d->upstair_list[i].x == x && d->upstair_list[i].y == y) {
						valid = 0;
					}
				}
				
				for (j = 0; j < i; j++) {
					if (d->downstair_list[i].x == x && d->downstair_list[i].y == y) {
						valid = 0;
					}
				}
				// If not valid, keep trying
				if (!valid) continue;
				
				d->downstair_list[i].x = x;
				d->downstair_list[i].y = y;
				found_pos = 1;
			}
		}
	}
}

void generate_corridors(dungeon* d) {
	int i, j;
	
	int* connected = malloc(d->room_count * sizeof(int));
	for (i = 0; i < d->room_count; i++) {
		connected[i] = 0;
	}
	
	connected[0] = 1;
	
	while(1) {
		// Check if all the rooms are connected
		int flag = 1;
		for (i = 0; i < d->room_count; i++) {
			if (!connected[i]) flag = 0;
		}
		if (flag) break;
		
		// Find the closest connected/disconnected pair of rooms
		int minDistFact = 10000;
		int room1 = 0;
		int room2 = 0;
		for (i = 0; i < d->room_count; i++) {
			if (connected[i]) continue;
			for (j = 0; j < d->room_count; j++) {
				if (!connected[j]) continue;
				
				// Compare distance, no need to square root since we
				// don't care about the actual distance
				int xFact = d->room_list[j].x - d->room_list[i].x;
				int yFact = d->room_list[j].y - d->room_list[i].y;
				int distFact = xFact * xFact + yFact * yFact;
				if (distFact < minDistFact) {
					minDistFact = distFact;
					room1 = i; //disconnected room
					room2 = j; //connected room
				}
			}
		}
		
		// Connect the rooms with corridors
		connect_rooms(d, room1, room2);
		connected[room1] = 1;
	}
	
	free(connected);
}

void connect_rooms(dungeon* d, int room1, int room2) {
	int i;
	
	// Start position
	int currX = randrange(d->room_list[room1].x, d->room_list[room1].x + d->room_list[room1].w - 1);
	int currY = randrange(d->room_list[room1].y, d->room_list[room1].y + d->room_list[room1].h - 1);

	// End position
	int targetX	= randrange(d->room_list[room2].x, d->room_list[room2].x + d->room_list[room2].w - 1);
	int targetY = randrange(d->room_list[room2].y, d->room_list[room2].y + d->room_list[room2].h - 1);

	while(1) {
		// Where we need to travel to get there
		int dx = targetX - currX;
		int dy = targetY - currY;

		// Select which direction and how far to go
		if (randchance(0.5)) {
			dx = 0;
			dy = sign(dy) * randrange(0, abs(dy)/2+1);
		} else {
			dy = 0;
			dx = sign(dx) * randrange(0, abs(dx)/2+1);
		}

		// Number of iterations in this leg of the corridor
		int dist = abs(dx + dy);

		// Draw each cell along the way, avoiding rooms
		// If we cross an existing corridor, exit - it is connected
		for (i = 0; i < dist; i++) {
			currX += sign(dx);
			currY += sign(dy);
			
			d->hardness[currY][currX] = 0;
		}

		// Once we have reached our target, exit
		if (currX == targetX && currY == targetY) {
			return;
		}
	}
}

void generate_rooms(dungeon* d) {
	int i, j, k;
	
	int room_eligible[MAP_HEIGHT][MAP_WIDTH];
	for (i = 0; i < MAP_HEIGHT; i++) {
		for (j = 0; j < MAP_WIDTH; j++) {
			room_eligible[i][j] = !(i == 0 || i == (MAP_HEIGHT-1) || j == 0 || j == (MAP_WIDTH-1));
		}
	}
	
	// Choose how many rooms to make
	int rooms_to_make = randrange(MIN_ROOMS, MAX_ROOMS);
	
	// Initialize the dungeon with that information
	d->room_count = rooms_to_make;
	d->room_list = malloc(rooms_to_make * sizeof(room));
	
	for (k = 0; k < rooms_to_make; k++) {
		int foundRoom = 0;
		int x, y, w, h;
		
		// Until we have found a valid position for the room
		while(!foundRoom) {
			// Generate random parameters for the room
			w = randrange(4, 10);
			h = randrange(3, 8);
			x = randrange(1, MAP_WIDTH - w);
			y = randrange(1, MAP_HEIGHT - h);
			
			// Assume that this is good
			foundRoom = 1;
			
			// Check every cell in the new room to see if it is eligible
			// If not, set the flag to false so we will try again
			for (i = y; i < y+h; i++) {
				for (j = x; j < x+w; j++) {
					if (!room_eligible[i][j]) foundRoom = 0;
				}
			}
		}
		
		// Save the parameters of the room in the array
		d->room_list[k].x = x;
		d->room_list[k].y = y;
		d->room_list[k].w = w;
		d->room_list[k].h = h;
		
		// Mark this room and the border around it as ineligible for room placement
		for (i = y-1; i < y+h+1; i++) {
			for (j = x-1; j < x+w+1; j++) {
				room_eligible[i][j] = 0;
			}
		}
		
		// Mark the cells in the map as room cells
		for (i = y; i < y+h; i++) {
			for (j = x; j < x+w; j++) {
				d->hardness[i][j] = 0;
			}
		}
	}
}

char get_map_char(dungeon *d, int y, int x) {
	int i;
	
	// Check each position from top down, i.e. check the layers which would
	// render over other layers first.
	
	if (y == 0 || y == (MAP_HEIGHT-1) || x == 0 || x == (MAP_WIDTH-1)) {
		if (y == 0 || y == (MAP_HEIGHT - 1)) return '-';
		else return '|';
	}
	
	if (x == d->player_pos.x && y == d->player_pos.y) return '@';
	
	for (i = 0; i < d->upstair_count; i++) {
		if (x == d->upstair_list[i].x && y == d->upstair_list[i].y) {
			return '<';
		}
	}
	
	for (i = 0; i < d->downstair_count; i++) {
		if (x == d->downstair_list[i].x && y == d->downstair_list[i].y) {
			return '>';
		}
	}
	
	for (i = 0; i < d->room_count; i++) {
		room* r = d->room_list + i;
		if (x >= r->x && x < r->x + r->w && y >= r->y && y < r->y + r->h) {
			return '.';
		}
	}
	
	if (d->hardness[y][x] == 0) {
		return '#';
	}
	
	return ' ';
}

void draw_map(dungeon* d) {
	int i, j;
	for (i = 0; i < MAP_HEIGHT; i++) {
		for (j = 0; j < MAP_WIDTH; j++) {
			printf("%c", get_map_char(d, i, j));
		}
		printf("\n");
	}
}

int randrange(int min, int max) {
	int range = max - min + 1;
	return (rand() % range) + min;
}

int randchance(double prob) {
	int resolution = RAND_MAX;
	return (rand() % resolution) < (prob * resolution);
}

int sign(int x) {
	if (x > 0) return 1;
	if (x < 0) return -1;
	return 0;
}
