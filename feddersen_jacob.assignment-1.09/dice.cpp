#include <cstdlib>

#include "dice.h"
#include "util.h"

int32_t dice::roll(void) const
{
  int32_t total;
  uint32_t i;

  total = base;

  if (sides) {
    for (i = 0; i < number; i++) {
      total += randrange(1, sides);
    }
  }

  return total;
}

std::ostream &dice::print(std::ostream &o)
{
  return o << base << '+' << number << 'd' << sides;
}

std::string dice::to_string()
{
  return std::to_string(base) + "+" + std::to_string(number) + "d" + std::to_string(sides);
}

std::ostream &operator<<(std::ostream &o, dice &d)
{
  return d.print(o);
}
