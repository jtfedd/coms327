#include <ncurses.h>
#include <cstdlib>
#include <vector>
#include <cstdint>
#include <cstring>

#include "character.h"
#include "dungeon.h"
#include "draw_dungeon.h"
#include "distance_map.h"
#include "line_of_sight.h"
#include "save_load.h"
#include "util.h"

void regen_dungeon(dungeon &d, heap_t *h, int nummon) {
  // Save the PC instance
  character *c = d.characters[d.player_pos.y][d.player_pos.x];
  d.characters[d.player_pos.y][d.player_pos.x] = NULL;
  
  heap_delete(h);
  
  d.free_data();
  
  d.gen_random_dungeon();
  
  // Delete the new PC that was created
  delete d.characters[d.player_pos.y][d.player_pos.x];
  // Substitute the old PC
  d.characters[d.player_pos.y][d.player_pos.x] = c;
  c->pos = d.player_pos;
  c->next_turn = 0;
  
  d.gen_monsters(nummon);
  d.gen_items(randrange(10, ITEMS_PER_LEVEL));
  
  d.init_turn_heap(h);
  calc_line_of_sight(d);
}

int main(int argc, char* argv[]) {  
  int i;
  
  args args;
  parse_args(argc, argv, &args);
  
  srand(args.seed);
  
  dungeon d;
  
  if (args.load) {
    load_dungeon(d);
  } else {
    d.gen_random_dungeon();
  }
  
  d.gen_monsters(args.nummon);
  d.gen_items(randrange(10, ITEMS_PER_LEVEL));

  
  if (args.save) {
    save_dungeon(d);
  }
  
  init_screen();
  
  display_message("Using seed: %d", args.seed);

  heap_t h;
  d.init_turn_heap(&h);
  
  int game_won = 0;
  
  // Initialize line of sight
  calc_line_of_sight(d);
  
  while(1) {
    character *c = (character *) heap_remove_min(&h);
    int clear_message = 1;
    
    // If the character is not alive, then free its memory
    if (!c->alive) {
      if (c->is_player()) {
        delete c;
        game_won = 0;
        break;
      } else {
        delete c;
        continue;
      }
    }
    
    // The character decides where to move next
    position next_move;
    if (!c->is_player()) {
      next_move = ((monster*)c)->monster_move(d);
    } else {
      d.draw();
      refresh_screen();
      
      int key;
      int handled_move = 0;
      while(!handled_move) {
        key = getch();
        
        if (key == -1) {
          d.draw();
          refresh_screen();
          continue;
        }
        
        if (key == 'Q' || key == 'q') {
          game_won = -1;
          break;
        }

        next_move = c->pos;

        if (key == '7' || key == 'y') {
          next_move.x--;
          next_move.y--;
          handled_move = 1;
        }

        if (key == '8' || key == 'k') {
          next_move.y--;
          handled_move = 1;
        }

        if (key == '9' || key == 'u') {
          next_move.y--;
          next_move.x++;
          handled_move = 1;
        }

        if (key == '6' || key == 'l') {
          next_move.x++;
          handled_move = 1;
        }

        if (key == '3' || key == 'n') {
          next_move.x++;
          next_move.y++;
          handled_move = 1;
        }

        if (key == '2' || key == 'j') {
          next_move.y++;
          handled_move = 1;
        }

        if (key == '1' || key == 'b') {
          next_move.x--;
          next_move.y++;
          handled_move = 1;
        }

        if (key == '4' || key == 'h') {
          next_move.x--;
          handled_move = 1;
        }

        if (key == '5' || key == ' ' || key == '.') {
          handled_move = 1;
        }
        
        if (key == ',') {
          ((player_character *)c)->pick_up(d.items[c->pos.y][c->pos.x]);
        }
        
        if (key == 'w') {
          ((player_character *)c)->wear_item();
        }
        
        if (key == 't') {
          item *dropped = ((player_character *)c)->take_off();
          if (dropped) {
            d.items[c->pos.y][c->pos.x].push_back(dropped);
          }
        }
        
        if (key == 'd') {
          item *dropped = ((player_character *)c)->drop_item();
          if (dropped) {
            d.items[c->pos.y][c->pos.x].push_back(dropped);
          }
        }
        
        if (key == 'x') {
          ((player_character *)c)->expunge();
        }
        
        if (key == 'i') {
          ((player_character *)c)->list_inventory();
        }
        
        if (key == 'e') {
          ((player_character *)c)->list_equipment();
        }
        
        if (key == 'I') {
          ((player_character *)c)->inspect_item();
        }
        
        if (key == 'L') {
          d.look_at_monsters();
        }
        
        if (key == 'm') {
          if (d.display_monster_list()) {
            game_won = -1;
            break;
          }
          d.draw();
          refresh_screen();
        }
        
        if (key == 'g') {
          if (d.targeting_mode()) {
            game_won = -1;
            break;
          }
          calc_line_of_sight(d);
          d.draw();
          refresh_screen();
        }
        
        if (key == 'f') {
          d.toggle_fog_of_war();
          d.draw();
          refresh_screen();
        }
        
        if (key == '>') {
          for (i = 0; i < d.downstair_count; i++) {
            if (d.downstair_list[i].x == d.player_pos.x && d.downstair_list[i].y == d.player_pos.y) {
              regen_dungeon(d, &h, args.nummon);
              handled_move = 2;
              d.draw();
              refresh_screen();
            }
          }
          if (!handled_move) {
            display_message("%s", "Can't go downstairs there!");
            refresh_screen();
          }
        }
        
        if (key == '<') {
          for (i = 0; i < d.upstair_count; i++) {
            if (d.upstair_list[i] == d.player_pos) {
              regen_dungeon(d, &h, args.nummon);
              handled_move = 2;
              d.draw();
              refresh_screen();
            }
          }
          if (!handled_move) {
            display_message("%s", "Can't go upstairs there!");
            refresh_screen();
          }
        }
      }
      
      if (handled_move == 2) {
        continue;
      }
    }
    
    if (game_won == -1) {
      break;
    }
    
    bool moved = false;
        
    // If we are attacking another character
    if (d.characters[next_move.y][next_move.x] && d.characters[next_move.y][next_move.x] != c) {
      moved = true;
      character *attacked = d.characters[next_move.y][next_move.x];
      
      if (attacked->is_player() != c->is_player()) {
        // Attack!
        int damage = c->get_damage();
        if (c->is_player()) {
          display_message("You smite the %s for %d damage", ((monster*)attacked)->get_description()->name.c_str(), damage);
          clear_message = 0;
        }
        attacked->hitpoints -= damage;
        if (attacked->hitpoints < 0) {
          if (attacked->is_player()) {
            game_won = 0;
            break;
          } else {
            monster *m = (monster *)attacked;
            m->die();
            m->alive = 0;
            d.characters[next_move.y][next_move.x] = NULL;
            
            if (m->has_characteristic(NPC_BOSS)) {
              delete m;
              game_won = 1;
              break;
            }
          }
        }
      } else {
        // Move the other monster out of the way
        d.characters[c->pos.y][c->pos.x] = NULL;
        bool found_position = false;
        position new_pos;
        while (!found_position) {
          new_pos.x = randrange(attacked->pos.x - 1, attacked->pos.x + 1);
          new_pos.y = randrange(attacked->pos.y - 1, attacked->pos.y + 1);
          
          if (!d.hardness[new_pos.y][new_pos.x] && !d.characters[new_pos.y][new_pos.x]) found_position = true;
        }
        
        d.characters[new_pos.y][new_pos.x] = attacked;
        attacked->pos = new_pos;
        d.characters[next_move.y][next_move.x] = c;
        c->pos = next_move;
      }
    }
    
    // Check for rock and tunneling
    if (d.hardness[next_move.y][next_move.x] && d.hardness[next_move.y][next_move.x] < 255 && c->has_characteristic(NPC_TUNNEL)) {
      if (d.hardness[next_move.y][next_move.x] > 85) {
        d.hardness[next_move.y][next_move.x] -= 85;
      } else if (d.hardness[next_move.y][next_move.x] > 0) {
        d.hardness[next_move.y][next_move.x] = 0;
      }
      
      // Update the line of sight
      calc_line_of_sight(d);
    }
    
    // If the space we want to move to has hardness zero, move there
    if (d.hardness[next_move.y][next_move.x] == 0 && !d.characters[next_move.y][next_move.x] && !moved) {
      d.characters[c->pos.y][c->pos.x] = NULL;
      d.characters[next_move.y][next_move.x] = c;
      c->pos = next_move;
      if (c->is_player()) {
        d.player_pos = next_move;
      }
    } else if (c->is_player() && d.hardness[next_move.y][next_move.x] != 0) {
      display_message("%s", "Ouch, that was a wall.");
      clear_message = 0;
    }
    
    // Update the character's turn and reinsert into heap
    c->next_turn = c->next_turn + (1000 / c->get_speed());
    heap_insert(&h, c);
    
    // If the player moved, update line of sight, redraw, and sleep for the timestep
    if (c->is_player()) {
      calc_line_of_sight(d);
      if (clear_message) {
        display_message("");
      }
    }
  }
  
  if (game_won == 1) {
    display_win();
  } else if (game_won == 0) {
    display_lose();
  }
  
  destroy_screen();
  
  heap_delete(&h);
}
