#ifndef SAVE_LOAD_H
#define SAVE_LOAD_H

#include <cstdint>

#include "dungeon.h"

#define FILETYPE_MARKER "RLG327-S2019"
#define VERSION 0

#define FILENAME "/.rlg327/dungeon"

/**
 * Load the dungeon from ~/.rlg327/dungeon
 */
void load_dungeon(dungeon &d);

/**
 * Save the dungeon to ~/.rlg327/dungeon
 */
void save_dungeon(dungeon &d);

/**
 * Opens the save file ~/.rlg327/dungeon
 */
FILE* open_save_file(char* mode);

/**
 * Helper functions for load/save with endianness
 */
void read_short(FILE* f, uint16_t* val);
void read_int(FILE* f, uint32_t* val);
void write_short(FILE* f, uint16_t val);
void write_int(FILE* f, uint32_t val);

#endif
