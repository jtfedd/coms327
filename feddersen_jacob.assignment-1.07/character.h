#ifndef CHARACTER_H
#define CHARACTER_H

#include <cstdint>

#include "heap.h"
#include "util.h"
#include "dungeon.h"

#define SMART   0x00000001
#define TELE    0x00000002
#define TUNNEL  0x00000004
#define ERRATIC 0x00000008

class dungeon;

class character {
 public:
  position pos;
  int speed;
  int seq_num;
  int next_turn;
  int alive;
  char symbol;
	character(uint8_t x, uint8_t y);
	virtual ~character();
	
	virtual bool is_player();
	virtual int has_characteristic(int bit);
};

class player_character : public character {
 public:
	player_character(uint8_t x, uint8_t y);
	
	bool is_player();
	int has_characteristic(int bit);
};

class monster : public character {
 private:
	uint32_t characteristics;
	position last_seen;
	
	position next_monster_move_tunnel_erratic(dungeon &d);
	position next_monster_move_erratic(dungeon &d);
	position next_monster_move_smart(dungeon &d);
	position next_monster_move_dumb(dungeon &d);
	
 public:
	monster(uint8_t x, uint8_t y);
	
	bool is_player();
	int has_characteristic(int bit);
	
	position monster_move(dungeon &d);
	
};

void init_character_turn_heap(heap_t *h);

#endif
