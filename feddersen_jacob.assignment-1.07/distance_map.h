#ifndef DISTANCE_MAP_H
#define DISTANCE_MAP_H

#include <climits>

#include "dungeon.h"

/**
 * Dijkstra's Algorithm - Find the shortest paths in the dungeon to the player position
 * hardness_limit - the smallest hardness value that the monsters cannot pass through
 *
 * Algorithm is modified from Dr. Sheaffer's Dijkstra code using his provided heap.
 */
void dijkstra_from_pos(dungeon &d, uint32_t dist[MAP_HEIGHT][MAP_WIDTH], int hardness_limit, int start_x, int start_y);

#endif
