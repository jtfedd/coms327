#ifndef MONSTER_DESC_H
#define MONSTER_DESC_H

#include <vector>
#include <string>
#include "util.h"

#define MONSTER_FILE "/.rlg327/monster_desc.txt"

class monster_desc {
 private:
	std::string name;
	std::string description;
	std::vector<std::string> colors;
	dice *speed;
	std::vector<std::string> abilities;
	dice *hitpoints;
	dice *attack_damage;
	char symbol;
	int rarity;

 public:
	monster_desc();
	~monster_desc();
	
	static std::vector<monster_desc *> parse_monster_desc_file();
	static void print_monster_desc(std::vector<monster_desc *> descriptions);
};

#endif