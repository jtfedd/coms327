#ifndef DRAW_DUNGEON_H
#define DRAW_DUNGEON_H

#include <cstdint>

#include "dungeon.h"

#define SCREEN_WIDTH 80
#define SCREEN_HEIGHT 24

/**
 * Draw the dungeon
 */
void init_screen();
void display_message(const char *fmt, ...);
void refresh_screen();
void destroy_screen();

#endif
