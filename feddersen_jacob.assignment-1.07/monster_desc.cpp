#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>
#include <cstdlib>

#include "monster_desc.h"

using namespace std;

monster_desc::monster_desc() {
	speed = nullptr;
	hitpoints = nullptr;
	attack_damage = nullptr;
}

monster_desc::~monster_desc() {
	if (speed) delete speed;
	if (hitpoints) delete hitpoints;
	if (attack_damage) delete attack_damage;
}

vector<monster_desc *> monster_desc::parse_monster_desc_file() {
	char *home = getenv("HOME");
	char *path;

	path = (char *) malloc(strlen(home) + strlen(MONSTER_FILE) + 1);

	strcpy(path, home);
	strcat(path, MONSTER_FILE);
	
	ifstream f;
	f.open(path);
	
	free(path);
	
	string line;
	getline(f, line);
	if (line != "RLG327 MONSTER DESCRIPTION 1") {
		cout << "Invalid Monster Description File: " << line << endl;
		exit(-1);
	}
	
	vector<monster_desc *> descriptions;
	
	while (true) {
		bool eof = false;
		while (true) {
			if (getline(f, line)) {
				if (line == "BEGIN MONSTER") break;
			} else {
				eof = true;
				break;
			}
		}
		if (eof) break;
		
		monster_desc *m = new monster_desc;
		
		bool fields[9];
		for (int i = 0; i < 9; i++) {
			fields[i] = false;
		}
		bool valid = true;
		
		while (true) {
			getline(f, line);
			
			if (line == "END") {
				// Check that all fields are satisfied
				bool valid = true;
				for (int i = 0; i < 9; i++) {
					if (!fields[i]) valid = false;
				}
				
				if (valid) {
					descriptions.push_back(m);
					break;
				} else {
					delete(m);
					break;
				}
			}
			
			//bool valid = true;
			
			stringstream sline(line);
			string token;
			sline >> token;
			
			if (token == "NAME") {
				if (fields[0]) valid = false;
				fields[0] = true;
				// Get rid of the space
				sline.get();
				getline(sline, m->name);
			}
			
			else if (token == "DESC") {
				if (fields[1]) valid = false;
				else {
					fields[1] = true;
					bool firstline = true;
					while(true) {
						getline(f, line);
						if (line == ".") break;
						if (!firstline) {
							m->description += "\n";
						} else {
							firstline = false;
						}
						m->description += line;
					}
				}
			}
			
			else if (token == "SYMB") {
				if (fields[2]) valid = false;
				else {
					fields[2] = true;
					sline >> m->symbol;
				}
			}
			
			else if (token == "RRTY") {
				if (fields[3]) valid = false;
				else {
					fields[3] = true;
					sline >> m->rarity;
				}
			}
			
			else if (token == "SPEED") {
				if (fields[4]) valid = false;
				else {
					fields[4] = true;
					string diceValue;
					sline >> diceValue;
					int b, d, s;
					if (sscanf(diceValue.c_str(), "%d+%dd%d", &b, &d, &s) != 3) valid = false;
					m->speed = new dice(b, d, s);
				}
			}
			
			else if (token == "HP") {
				if (fields[5]) valid = false;
				else {
					fields[5] = true;
					string diceValue;
					sline >> diceValue;
					int b, d, s;
					if (sscanf(diceValue.c_str(), "%d+%dd%d", &b, &d, &s) != 3) break;
					m->hitpoints = new dice(b, d, s);
				}
			}
			
			else if (token == "DAM") {
				if (fields[6]) valid = false;
				else {
					fields[6] = true;
					string diceValue;
					sline >> diceValue;
					int b, d, s;
					if (sscanf(diceValue.c_str(), "%d+%dd%d", &b, &d, &s) != 3) break;
					m->attack_damage = new dice(b, d, s);
				}
			}
			
			else if (token == "COLOR") {
				if (fields[7]) valid = false;
				else {
					fields[7] = true;
					string color;
					while(sline >> color) {
						m->colors.push_back(color);
					}
				}
			}
			
			else if (token == "ABIL") {
				if (fields[8]) break;
				else {
					fields[8] = true;
					string abil;
					while(sline >> abil) {
						m->abilities.push_back(abil);
					}
				}
			}
			
			else valid = false;
			
			if (!valid) {
				delete(m);
				break;
			}
		}
	}
	
	f.close();
	
	return descriptions;
}

void monster_desc::print_monster_desc(vector<monster_desc *> descriptions) {
	for (unsigned int i = 0; i < descriptions.size(); i++) {
		if (i != 0) cout << endl;
		cout << descriptions[i]->name << endl;
		cout << descriptions[i]->description << endl;
		cout << descriptions[i]->symbol << endl;
		
		for (unsigned int j = 0; j < descriptions[i]->colors.size(); j++) {
			if (j != 0) cout << " ";
			cout << descriptions[i]->colors[j];
		}
		cout << endl;
		
		descriptions[i]->speed->print();
		
		for (unsigned int j = 0; j < descriptions[i]->abilities.size(); j++) {
			if (j != 0) cout << " ";
			cout << descriptions[i]->abilities[j];
		}
		cout << endl;
		
		descriptions[i]->hitpoints->print();
		descriptions[i]->attack_damage->print();
		cout << descriptions[i]->rarity << endl;
	}
}
