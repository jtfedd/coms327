#include <ncurses.h>
#include <cstdint>
#include <climits>
#include <cstdarg>

#include "dungeon.h"
#include "draw_dungeon.h"

char monster_chars[] = "0123456789abcdef";

void init_screen() {
	// Initialize ncurses screen
	initscr();
	set_escdelay(0);
	
	// Unbuffered input
  cbreak();
	
	// Don't echo back characters that are typed
  noecho();
	
	// Hide the cursor
	curs_set(0);
	nonl();
	intrflush(stdscr, FALSE);
	
	// Make it so keypad characters do what we expect
	keypad(stdscr, TRUE);
	
	// Clear the screen
  clear();
}

void destroy_screen() {
	endwin();
}

void display_message(const char *fmt, ...) {
	move(0, 0);
	clrtoeol();
	
	move(0, 0);
	
	va_list args;
	va_start(args, fmt);
	vwprintw(stdscr, fmt, args);
	va_end(args);
}

void refresh_screen() {
	refresh();
}
