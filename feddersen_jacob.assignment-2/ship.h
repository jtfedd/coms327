#ifndef SHIP_H
#define SHIP_H

#define SHIP_PLAYER 0
#define SHIP_OPPONENT 1

#include <ncurses.h>
#include <string>

#define UP    0
#define LEFT  1
#define DOWN  2
#define RIGHT 3

class ship {
private:
  int x;
  int y;
  int dir;
  
  int maxHP;
  int HP;
  
  std::string name;
public:
  ship(int x, int y, int dir, int maxHP, std::string name) : x(x), y(y), dir(dir), maxHP(maxHP), HP(maxHP), name(name) {}
  ~ship() {}
  
  bool contains(int x, int y);
  void hit();
  bool is_sunk();
  
  int get_x();
  int get_y();
  int get_dir();
  
  void print_ship(int x, int y, int side);
};

#endif