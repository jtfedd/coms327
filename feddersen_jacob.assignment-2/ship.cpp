#include <ncurses.h>

#include "ship.h"
#include "colors.h"

bool ship::contains(int x, int y) {
  int dx = 0;
  int dy = 0;
  
  if (dir == UP) {
    dy = -1;
  } else if (dir == DOWN) {
    dy = 1;
  } else if (dir == LEFT) {
    dx = -1;
  } else if (dir == RIGHT) {
    dx = 1;
  }
  
  for (int i = 0; i < maxHP; i++) {
    if (this->x + (i * dx) == x && this->y + (i * dy) == y) {
      return true;
    }
  }
  
  return false;
}

void ship::hit() {
  HP--;
}

bool ship::is_sunk() {
  return HP <= 0;
}

int ship::get_x() {
  return x;
}

int ship::get_y() {
  return y;
}

int ship::get_dir() {
  return dir;
}

void ship::print_ship(int x, int y, int side) {
  if (is_sunk()) {
    attron(COLOR_PAIR(TEXT_RED));
  } else {
    attron(COLOR_PAIR(TEXT_WHITE));
  }
  
  mvprintw(y, x, name.c_str());
  
  if (is_sunk()) {
    attroff(COLOR_PAIR(TEXT_RED));
  } else {
    attroff(COLOR_PAIR(TEXT_WHITE));
  }
  
  if (side==SHIP_OPPONENT) return;
  
  attron(COLOR_PAIR(TEXT_GREEN));
  for (int i = maxHP; i > (maxHP - HP); i--) {
    mvaddch(y, x+23-i, '*');
  }
  attroff(COLOR_PAIR(TEXT_GREEN));
  
  attron(COLOR_PAIR(TEXT_RED));
  for (int i = maxHP-HP; i > 0; i--) {
    mvaddch(y, x+23-(HP)-i, '*');
  }
  attroff(COLOR_PAIR(TEXT_RED));
}
