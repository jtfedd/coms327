#ifndef UTIL_H
#define UTIL_H

#include <ncurses.h>
#include <string>

#include "colors.h"

void init_screen();
void destroy_screen();
void enable_raw_input();
void enable_string_input();
std::string getstring();

int randrange(int min, int max);
int sign(int x);

void clear_message();
void display_message_1(std::string message);
void display_message_2(std::string message);

#endif
