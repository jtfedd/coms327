#ifndef NETWORKING_H
#define NETWORKING_H

#include <ncurses.h>
#include <string>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <ifaddrs.h>

#include "util.h"

#define BATTLESHIP_PORT 36127

class connection {
public:
  virtual ~connection() {}
  
  virtual std::string exchange_message(const std::string &message) = 0;
};

class server_connection : public connection {
private:
  int server_fd, server_sock;
  struct sockaddr_in address;
  int addrlen;
public:
  server_connection();
  ~server_connection();
  
  std::string exchange_message(const std::string &message);
};

class client_connection : public connection {
private:
  int client_sock;
  struct sockaddr_in address;
  struct sockaddr_in serv_addr;
public:
  client_connection();
  ~client_connection();
  
  std::string exchange_message(const std::string &message);
};

#endif