#include "asciiart.h"

// ASCII art from http://ascii.co.uk/art/battleship
// and http://patorjk.com/software/taag/#p=display&f=Cyberlarge&t=Battleship%0A

const char *battleship_ascii[] = {
"______  _______ _______ _______        _______ _______ _     _ _____  _____", 
"|_____] |_____|    |       |    |      |______ |______ |_____|   |   |_____]",
"|_____] |     |    |       |    |_____ |______ ______| |     | __|__ |      ",
"                                                                            ",
"                                      # #  ( )",
"                                   ___#_#___|__",
"                               _  |____________|  _",
"                        _=====| | |            | | |==== _",
"                  =====| |.---------------------------. | |====",
"    <--------------------'   .  .  .  .  .  .  .  .   '--------------/",
"      \\                                                             /",
"       \\___________________________________________________________/",
"   wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww",
" wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww",
"   wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww ",
};

void print_battleship() {
  for (int i = 0; i < 15; i++) {
    mvprintw(1+i, 2, battleship_ascii[i]);
  }
}
