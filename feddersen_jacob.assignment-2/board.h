#ifndef BOARD_H
#define BOARD_H

#define PLAYER 0
#define OPPONENT 1

#include <ncurses.h>
#include <vector>
#include <string>
#include <algorithm>

#include "ship.h"
#include "colors.h"
#include "util.h"

class board {
private:
  int side;

  int get_position_color(int x, int y);
  
  std::vector<ship *> ships;
  
  bool guesses[10][10];
  
  bool show_cursor;
  int cursor_x;
  int cursor_y;
    
public:
  board(int side);
  board(int side, std::string &board_description);
  ~board();
  
  std::string get_description();
  
  void make_move(std::string position);
  std::string get_move();
  bool has_lost();
  
  void draw();
  void draw_game_over();
};

#endif